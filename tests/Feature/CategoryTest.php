<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Category;

class CategoryTest extends TestCase
{
   use RefreshDatabase;
   use DatabaseMigrations;
   protected $data=["name"=>"Main Menu"];
    public function setUp(): void
    {
        parent::setUp();

        // you can call
      //  $this->artisan('db:seed');

    }
    public function addtoCategoryToDataBase(){
        $this->data=["name"=>"Main Menu"];
        $response = $this->post('/category',$this->data);
    }

    /**
     * A basic feature test example.
     *@group  CategoryTest
     * @return void
     */
    public function test_can_add_category()
    {
        $this->addtoCategoryToDataBase();
        $this->assertDatabaseHas('categories', $this->data);
        $response = $this->get('/category');
        $response->assertSee($this->data["name"]);

    }

    /**
     * A basic feature test example.
     *@group  CategoryTest
     * @return void
     */
    public function test_can_not_add_category_with_invalid_name()
    {
        $data=["name"=>""];
        $response = $this->post('/category',$data)
        ->assertSessionHasErrors(["name"]);
        // min 2 chacters
        $data=["name"=>"M"];
        $response = $this->post('/category',$data)
        ->assertSessionHasErrors(["name"]);
        // max 40 characters
        $data=["name"=>"123456789012345678901234567890123456789012345678901234567890"];
        $response = $this->post('/category',$data)
        ->assertSessionHasErrors(["name"]);

    }


    /**
     * A basic feature test example.
     *@group  CategoryTest
     * @return void
     */
    public function test_can_update_category()
    {
        $this->addtoCategoryToDataBase();
        $data=["name"=>"Updated Menu"];
        $category=Category::get()->random()->first();

        $response = $this->put('/category/'.$category->id,$data);
        $this->assertDatabaseHas('categories',  $data);
        $response = $this->get('/category');
        $response->assertSee($data["name"])
        ->assertDontSeeText($this->data["name"]);
    }

    /**
     * A basic feature test example.
     *@group  CategoryTest
     * @return void
     */
    public function test_can_delete_category()
    {
        $this->addtoCategoryToDataBase();
        $category=Category::get()->random()->first();

        $response = $this->delete('/category/'.$category->id);
        $this->assertDatabaseMissing('categories', ['name'=>$category->name]);
    }
}
