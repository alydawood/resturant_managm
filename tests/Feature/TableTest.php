<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Table;

class TableTest extends TestCase
{
    use RefreshDatabase;
   use DatabaseMigrations;
   protected $data=["name"=>"Main Menu"];
   public function setUp(): void
   {
       parent::setUp();

       // you can call
     //  $this->artisan('db:seed');

   }
   public function addTableToDataBase(){
       $this->data=["name"=>"Table 1A"];
       $response = $this->post('/tables',$this->data);
   }

    /**
     * A basic feature test example.
     *@group  TableTest
     * @return void
     */
    public function test_can_add_table()
    {
        $this->addTableToDataBase();
        $this->assertDatabaseHas('tables', $this->data);
        $response = $this->get('/tables')
        ->assertSee($this->data["name"]);
    }

    /**
     * A basic feature test example.
     *@group  TableTest
     * @return void
     */
    public function test_can_not_add_Table_with_invalid_name()
    {
        $data=["name"=>""];
        $response = $this->post('/tables',$data)
        ->assertSessionHasErrors(["name"]);
        // min 2 chacters
        $data=["name"=>"M"];
        $response = $this->post('/tables',$data)
        ->assertSessionHasErrors(["name"]);
        // max 40 characters
        $data=["name"=>"123456789012345678901234567890123456789012345678901234567890"];
        $response = $this->post('/category',$data)
        ->assertSessionHasErrors(["name"]);

    }

    /**
     * A basic feature test example.
     *@group  TableTest
     * @return void
     */
    public function test_can_update_table()
    {
        $this->addTableToDataBase();
        $data=["name"=>"Table Z1"];
        $table=Table::get()->random()->first();

        $response = $this->put('/tables/'.$table->id,$data);
        $this->assertDatabaseHas('tables',  $data);
        $response = $this->get('/tables');
        $response->assertSee($data["name"])
        ->assertDontSeeText($this->data["name"]);
    }

    /**
     * A basic feature test example.
     *@group  TableTest
     * @return void
     */
    public function test_can_delete_table()
    {
        $this->addTableToDataBase();
        $table=Table::get()->random()->first();

        $response = $this->delete('/tables/'.$table->id);
        $this->assertDatabaseMissing('tables', ['name'=>$table->name]);
    }
}
