import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export const store =new Vuex.Store({


    state:{
        table:null,
        selectedTableID:0,
        orderTotal:0,
        orders:[],
        reloadTables:0
    },
    getters:{
        getTable:state =>{
            return state.table;
        },
        getQtyByID:(state,id) =>{
            const index = state.orders.findIndex(order => order.id === id );
            if(index<0) return 0;
            return state.orders[index].qty;
        },
        getTableName:state =>{
            return state.table.name;
        },
        getTableID:state =>{
            return state.table.id;
        },
        getselectedTableID:state =>{
            return state.selectedTableID;
        },
        getOrder:state =>{
            return state.orders;
        },
        getOrderTotal:state =>{
            return state.orderTotal;
        },
        reloadTheTables:state =>{
            return state.reloadTables;
        },
    },
    mutations:{
       setTable:(state,obj) =>{
            state.table=obj;
            state.selectedTableID=obj.id;
        },
        setOrderTotal:(state,obj) =>{
            let lst=state.orders;
             let sum = lst.map(o => parseFloat(o.price * o.qty)).reduce((a, c) => { return a + c });
             state.orderTotal=sum.toFixed(2);
            },
        setOrder:(state,obj) =>{
        state.orders.push(obj);
        //let sum = lst.map(o => o.price).reduce((a, c) => { return a + c });
        //console.log(sum);
        },
        updateOrder:(state,obj) =>{
            const index = state.orders.findIndex(order => order.id === obj.id );
           // Vue.set(state.orders,index,obj)
            state.orders.splice(index,1,obj);
        },
        removeOrder:(state,obj) =>{
            const index = state.orders.findIndex(order => order.id === obj.id );
            state.orders.splice(index,1);
        },
        cancelOrder:(state) =>{
            state.orders=[];
            state.table=null;
        },
        reloadtables:(state)=>{
            state.reloadTables++;
        }

    },
    actions:{
        setTable:(context,obj) =>{
            context.commit('setTable',obj)
         },
         setOrder:(context,obj) =>{
            const index = context.state.orders.findIndex(order => order.id === obj.id );
            if(obj.qty<=0){context.commit('removeOrder',obj);}
            else if(index<0) context.commit('setOrder',obj);
            else  context.commit('updateOrder',obj);
            context.commit('setOrderTotal',obj);

         },
         cancelOrder:(context,obj) =>{
            context.commit('cancelOrder')
         },
         reloadtables:(context)=>{
            context.commit('reloadtables')
        }

    }
     });
