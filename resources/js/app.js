/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import {store} from './store/store';

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('table-component', require('./components/Table.vue').default);
Vue.component('tablelist-component', require('./components/TableList.vue').default);
Vue.component('menuitem-component', require('./components/MenuItem.vue').default);
Vue.component('menuitemlist-component', require('./components/MenuItemList.vue').default);
Vue.component('quantity-component', require('./components/Quantity.vue').default);
Vue.component('order-component', require('./components/Order.vue').default);
Vue.component('orderlist-component', require('./components/OrderList.vue').default);
Vue.component('chefitemplaced-component', require('./components/ChefItemPlaced.vue').default);
Vue.component('chefitempreparing-component', require('./components/ChefItemPreparing.vue').default);
Vue.component('chefitemlist-component', require('./components/ChefItemList.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#appt',
    store:store
});

