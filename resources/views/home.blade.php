@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                 <div class="card-header text-center">Please Make A Selection</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(Auth::user())
                    <div class="row text-center">
                         @include('inc.management')
                         @include('inc.order')
                         @include('inc.pay')
                         @include('inc.chef')
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('JSscripts')
<script>
    $( document ).ready(function() {
    $("body").addClass('registernbg');
  });
</script>
@endsection
