@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center pt-5 pb-5" style="background-color: #fff">

        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>

        <div class="col-md-8">
        <a type="button" class="btn btn-success" href="{{route('tables.create')}}">
            <i class="fas fa-plus-circle"></i>   Create New Table</a>
        <br/>
        <div class="row mt-5" style="width:70%">
          @foreach($tables as $table)
            <div class="col-sm-12 col-md-5 border-bottom">
                {{ Str::limit($table->name, 20) }}
            </div>
            <div class="col-sm-6 col-md-2 align-left border-bottom" >
                <a href="{{route('tables.edit',$table->id)}}" class="btn btn-warning btn-sm editbtn" >Edit</a>
            </div>
            <div class="col-sm-6 col-md-5  align-left border-bottom">
                <form action="{{route('tables.destroy',$table->id)}}" method="post">
                    @csrf
                    @method("DELETE")
                <div   class="btn btn-danger btn-sm tabledelete" style="color:#fff" tablename="{{$table->name}}">Delete</div>
                </form>

            </div>
            @endforeach
        </div>
    </div>
    </div>
  </div>

@endsection
@section('JSscripts')
<script>
    $( document ).ready(function() {

    $("body").addClass('tablebg');

    $('.tabledelete').click(function(){
         let tablename= $(this).attr('catname');
         let msg= `Are you sure you want to delete ${name}?`;
         let cm=confirm(msg);
         if(cm) $('form').submit();
    });

  });
</script>
@endsection
