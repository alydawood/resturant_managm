@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>
      <div class="col-md-8">
        <i class="fas fa-chair  oranged-text"></i> Edit Table
        <hr>
        @if($errors->any())
        <div class="errormessage">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif
        <form action="{{route('tables.update',$table->id)}}" method="POST">
            @csrf
            @method("PUT")
          <div class="form-group">
            <label for="tableName">Table Name</label>
            <input type="text" name="name" class="form-control" value="{{ old('name',$table->name)}}">
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection
