@extends('layouts.app')

@section('content')

<div class="container">
    <div id="appt">
       <div class="row">
         <div class="col-md-4" style="background-color: #fff">

        <tablelist-component :key='this.$store.getters.reloadTheTables' action="cashier">
        </tablelist-component>


         </div>

         <div class="col-md-8" style="background-color: #fff">
            <transition name="orderlist">
            <orderlist-component :payment="1"  v-if="this.$store.getters.getselectedTableID!=0"
            :key="this.$store.getters.getselectedTableID" :user_id="{{Auth::id()}}"></orderlist-component>
            </transition>
         </div>

       </div>


    </div>
    </div>
@endsection


@section('JSscripts')
<script>
    $( document ).ready(function() {
    $("body").addClass('registernbg');
  });
</script>
@endsection


