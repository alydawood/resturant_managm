@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center pt-5 pb-5" style="background-color: #fff">

        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>

        <div class="col-md-8">
        <a type="button" class="btn btn-success" href="{{route('category.create')}}">
            <i class="fas fa-plus-circle"></i>       Create Categories</a>
        <br/>
        <div class="row mt-5" style="width:70%">
          @foreach($categories as $category)
            <div class="col-sm-12 col-md-5 border-bottom">
                {{ Str::limit($category->name, 20) }}
            </div>
            <div class="col-sm-6 col-md-2 align-left border-bottom" >
                <a href="{{route('category.edit',$category->id)}}" class="btn btn-warning btn-sm editbtn" >Edit</a>
            </div>
            <div class="col-sm-6 col-md-5  align-left border-bottom">
                <form action="{{route('category.destroy',$category->id)}}" method="post">
                    @csrf
                    @method("DELETE")
                <div   class="btn btn-danger btn-sm catdelete" style="color:#fff" catname="{{$category->name}}">Delete</div>
                </form>

            </div>
            @endforeach
        </div>
    </div>
    </div>
  </div>

@endsection
@section('JSscripts')
<script>
    $( document ).ready(function() {

    $("body").addClass('categorybg');

    $('.catdelete').click(function(){
         let name= $(this).attr('catname');
         let msg= `Are you sure you want to delete ${name}?`;
         let cm=confirm(msg);
         if(cm) $('form').submit();
    });

  });
</script>
@endsection
