@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>
      <div class="col-md-8">
        <i class="fas fa-align-justify oranged-text"></i> Edit Category
        <hr>
        @if($errors->any())
        <div class="errormessage">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif
        <form action="{{route('category.update',$category->id)}}" method="POST">
            @csrf
            @method("PUT")
          <div class="form-group">
            <label for="categoryName">Category Name</label>
            <input type="text" name="name" class="form-control" placeholder="Category..." value="{{ old('name',$category->name)}}">
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection
