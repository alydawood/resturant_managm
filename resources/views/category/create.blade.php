@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center pt-5 pb-5" style="background-color: #fff">
        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>
      <div class="col-md-8">
        <i class="fas fa-align-justify oranged-text"></i> Create a Category
        <hr>
        @if($errors->any())
        <div class="errormessage">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif
        <form action="{{route('category.store')}}" method="POST">
          @csrf
          <div class="form-group">
            <label for="categoryName">Category Name</label>
            <input type="text" name="name" class="form-control" placeholder="Category..." value="{{ old('name')}}">
          </div>
          <button type="submit" class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('JSscripts')
<script>
    $( document ).ready(function() {
    $("body").addClass('categorybg');
  });
</script>
@endsection
