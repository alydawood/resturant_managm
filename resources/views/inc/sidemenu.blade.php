<div class="list-group">
    <a href="{{route('category.index')}}" class="list-group-item list-group-item-action"><i class="fas fa-align-justify orange-text"></i> Category</a>
    <a href="{{route('dishes.index')}}" class="list-group-item list-group-item-action"><i class="fas  fa-carrot orange-text"></i>&nbsp;Food</a>
    <a href="{{route('tables.index')}}" class="list-group-item list-group-item-action"><i class="fas fa-chair orange-text"></i> Table</a>
    <a href="{{route('register')}}" class="list-group-item list-group-item-action"><i class="fas fa-users-cog orange-text"></i> User</a>
</div>
