@if(Auth::user()->checkAdmin() || Auth::user()->checkServer())
<div class="col-sm-3">
    <a href="{{route('payments.index')}}">
        <h4>Pay</h4>
        <img width="50px" src="{{asset('img/cashier_manage_icon.png')}}"/>
    </a>
</div>
@endif
