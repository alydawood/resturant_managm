@extends('layouts.app')

@section('content')

<div class="container">
<div id="appt">
   <div class="row">
     <div class="col-md-4" style="background-color: #fff">

    <tablelist-component :key='this.$store.getters.reloadTheTables' action="order">
    </tablelist-component>

<orderlist-component :user_id="{{Auth::id()}}" :payment="0"></orderlist-component>
     </div>

     <div class="col-md-8" style="background-color: #fff">

        <menuitemlist-component>
        </menuitemlist-component>

     </div>

   </div>


</div>
</div>
@endsection

@section('JSscripts')
<script>
    $( document ).ready(function() {

    $("body").addClass('dishbg');



  });
</script>
@endsection
