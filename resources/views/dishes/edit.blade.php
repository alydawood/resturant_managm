@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center" >
        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>
      <div class="col-md-8" style="background-color: #fff">
        <i class="fas fa-carrot orange-text"></i>Update a Dish
        <hr>
        @if($errors->any())
        <div class="errormessage">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif
        <form action="{{route('dishes.update',$dish->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          @method("PUT")
          <div class="form-group">
            <label for="menuName">Dish Name</label>
            <input type="text" name="name" class="form-control" placeholder="Menu..." value="{{ old('name',$dish->name)}}">
          </div>
          <div class="form-group">
            <label for="name">Price</label>
            <input type="text" name="price" class="form-control"  style="width:100px" value="{{ old('price',$dish->price)}}">
          </div>


                 <label for="MenuImage">Image</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Upload</span>
            </div>
            <div class="custom-file">
              <input type="file" name="image" class="custom-file-input" id="inputGroupFile01">
              <label class="custom-file-label" for="inputGroupFile01">Choose File</label>
            </div>
          </div>

          <div class="form-group">
            <label for="Description">Description</label>
            <textarea name="description" class="form-control">
                {{ old('description',$dish->description)}}
            </textarea>
          </div>

          <div class="form-group">
            <label for="Category">Category</label>
            <select class="form-control" name="category_id">
                <option value="0">Select Category</option>
              @foreach ($categories as $category)
                <option value="{{$category->id}}" {{$dish->category_id === $category->id ? 'selected': ''}}>{{$category->name}}</option>

              @endforeach
            </select>
          </div>

          <button type="submit" class="btn btn-primary">Update</button>
        </form>
      </div>
    </div>
  </div>
@endsection
@section('JSscripts')
<script>
    $( document ).ready(function() {
    $("body").addClass('dishbg');
  });
</script>
@endsection
