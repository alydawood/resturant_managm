@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center pt-5 pb-5">
        <div class="col-md-4">
            @include('inc.sidemenu')
        </div>
      <div class="col-md-8 pt-5 pb-5" style="background-color: #fff">
        <i class="fas fa-carrot orange-text"></i>Dish
      <a href="{{route('dishes.create')}}" class="btn btn-success btn-sm float-right"><i class="fas fa-plus"></i> Create Dish</a>
        <hr>
        @if($errors->any())
        <div class="errormessage">
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif
        @foreach($dishes as $dish)
        <div class="row border-bottom" style="background-color: #fff">
            <div class="col-md-4">
                <img src="{{Storage::url('dish').'/'.$dish->image}}"  alt="{{$dish->name}}" width="100px" >
            </div>
            <div class="col-md-4">{{$dish->name}}</div>
            <div class="col-md-2">{{$dish->price}}</div>
            <div class="col-md-2">{{$dish->category->name}}</div>
            <div class="col-md-12">
                <button type="button" class="btn btn-link green-text" ><i class="far fa-eye"></i>View</button>
            <a class="btn btn-link oranged-text" href="{{route('dishes.edit',$dish->id)}}">
                    <i class="far fa-edit"></i>Update
                </a>
            <form action="{{route('dishes.destroy',$dish->id)}}" method="POST">
                <button type="button" class="btn btn-link red-text dishdelete"><i class="fas fa-trash-alt "
                    dishname="{{$dish->name}}"
                    ></i> Delete</button>
                    @csrf
                    @method("DELETE")

            </form>
            </div>

        </div>
        @endforeach





      </div>
    </div>
  </div>
@endsection
@section('JSscripts')
<script>
    $( document ).ready(function() {
    $("body").addClass('dishbg');
    $('.dishdelete').click(function(){
         let name= $(this).attr('dishname');
         let msg= `Are you sure you want to delete ${name}?`;
         let cm=confirm(msg);
         if(cm) $(this).parent().submit();
    });


  });
</script>
@endsection
