<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/categories','Category\CategoryController@jsoncategories',
)->name('dishes.jsoncategories');

Route::get('/tables','Table\TableController@jasontables',
)->name('tables.jasontables');

Route::get('/dishes/categories/{category_id}','Dish\DishController@jsondishes',
)->name('dishes.category');
Route::get('/dishes/{dish_id}','Dish\DishController@show',
)->name('dishes.show');


Route::resource('/orderdetails','Order\OrderDetailsController');
Route::get('/orderdetails/table/{table_id}','Order\OrderDetailsController@getsaleid',
)->name('orderdetails.table');
Route::get('/orderdetails/kitchen/orders','Order\OrderDetailsController@kitchenorders',
)->name('orderdetails.kitchenorders');


Route::post('/payments/method','Payment\PaymentController@method')->name('payments.method');

