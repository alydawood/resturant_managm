<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth','IsAdmin'])->group(function(){
    Route::get('/management', 'HomeController@management')->name('management');
    Route::resource('tables','Table\TableController');
    Route::resource('category','Category\CategoryController')->middleware('auth');
    Route::resource('dishes','Dish\DishController')->middleware('auth');

});
Route::middleware(['auth','IsChef'])->group(function(){
    Route::view('/chefs', 'chef.index')->name('chef.all');
});
Route::middleware(['auth','IsServer'])->group(function(){
    Route::get('/servers', function () {
        return view('servers.index');
    })->name('servers');
    Route::resource('payments','Payment\PaymentController');
});


Route::get('/', function () {
    return view('home');
})->middleware('auth');


Auth::routes();





Route::get('/home', 'HomeController@index')->name('home');



