<?php

namespace App\Http\Controllers\Dish;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Dish;
use App\Category;
use App\Http\Requests\CreateDishRequest;
use App\Http\Requests\UpdateDishRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ApiController;


class DishController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishes=Dish::all();
        $categories=Category::all();
        return view('dishes.index',compact("dishes","categories"));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dishes=Dish::all();
        $categories=Category::all();
        return view('dishes.create',compact("dishes","categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDishRequest $request)
    {
       $data=$request->all();
       $filename=   uniqid("dish").$request->file('image')->getClientOriginalName();
       $data["image"]=$filename;
       $request->file('image')->storeAs('public/dish/',$filename);
        Dish::create($data);
        return redirect()->route('dishes.index')->with('status', $request->name. " is saved successfully");;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dish=Dish::find($id);
        return $this->showOne($dish);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dish $dish)
    {
        $categories=Category::all();
        return view('dishes.edit',compact("dish","categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Dish $dish,UpdateDishRequest $request)
    {

        $data=$request->all();

        if($request->hasFile('image')){

            $filename=   uniqid("dish").$request->file('image')->getClientOriginalName();
            $data["image"]=$filename;
            $request->file('image')->storeAs('public/dish/',$filename);
            //Storage::delete('dish/'.$dish->image);
            Storage::disk('dish')->delete($dish->image);
            $data['image']= $filename;
        }
        $dish->update($data);
        return redirect()->route('dishes.index')->with('status', $data['name']. " is updated successfully");;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dish $dish)
    {
        $name=$dish->name;
        Storage::disk('dish')->delete($dish->image);
        $dish->delete();
        return redirect()->route('dishes.index')->with('status', $name. " is delete successfully");;
    }

    public function jsondishes($category_id)
    {

        $dishes=Dish::where('category_id','=',$category_id)->get();
        return $this->showall($dishes);

    }
}
