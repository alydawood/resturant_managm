<?php

namespace App\Http\Controllers\Order;

use App\Table;
use App\OrderDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Http\Requests\CreateOrderDetails;
use App\Http\Requests\UpdateOrderDetails;

class OrderDetailsController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "aaa";
    }

    public function kitchenorders(){
        $oderdetails=OrderDetails::whereIn('status', ['placed','preparing'])
        ->with("Dish")
        ->get();
        return $this->showAll($oderdetails,'200');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $data['server_name']="A";
        $data['status']="placed";
        $order=OrderDetails::create($data);
        $table=Table::find($data['table_id']);
        $table->status=0;
        $table->save();
        return $this->successResponse($table,'200');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders=OrderDetails::where("sale_id",$id)->with("dish")->get();
        return $this->successResponse($orders,'200');
    }
    public function getsaleid($id)
    {
        $orderd=OrderDetails::where("table_id","=",$id)
        ->where("status","=","placed")->get()->first();
        return $this->showOne($orderd,'200');
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderDetails $request, $id)
    {
        $orderdetails=OrderDetails::where('id','=',$id)->first();
      //  dd($orderdetails);
        $orderdetails->update([
         'status'=>   $request['status']
        ]);
       return $this->showOne($orderdetails,'200');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
