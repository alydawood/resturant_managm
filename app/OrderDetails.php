<?php

namespace App;

use App\Dish;
use App\Events\OrderPlaced;
use App\Events\OrderUpdated;
use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $guarded = ['id'];
    protected $with = ['Dish'];
    protected $dispatchesEvents=[
        'created'=>OrderPlaced::class,
        'updated'=>OrderUpdated::class,
      /*  'deleted'=>UserDeleted::class,
        */

    ];

    public function dish(){
        return $this->belongsTo(Dish::class);
    }
}
